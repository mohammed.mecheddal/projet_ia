Nom 	: MECHEDDAL 	|
Pr�nom 	: Mohammed	|
------------------------- 

#Random :
pour la probabilit� uniforme j'ai utilis� la fonction 
random.random() de la biblioth�que numpy 

#Minmax :

1.La fonction d� �evaluation qui s'appelle evaluer()
	parcour le plateau case par case pour compter
	les case qui contient des pions noir et 
	des pions blanc apr�s avoir initialiser les scores
	pour � la fin retourner une �valuation qui d�pent
	du joueur (noir ou blac).

2.L�algorithme minmax avec une profondeur d'essaye initiale = 5

3.Le compteur du nombre de fois que le Joueur utilise
	la m�thode jouer est repr�sant� par la variable compteurJouer
	cette derrniere est afficher � chauqe op�ration effectuer
	par l'un des joueur qui utilise le Minmax.

4.un compteur de temps physique avec la fonction time.time() 
	qui me retourne un flottant.
	j'ai utiliser deux variables temps_debut et temps_fin 
	pour d�but et fin de l'op�ration
	et j'ai fait la differance entre eux
	pour avoir la dur�e de l'op�ration que j'affiche � chaque fois
	dans le consol. Je me suis insp�rer de openclassroom
	https://openclassrooms.com/fr/courses/235344-apprenez-a-programmer-en-python/234041-exprimez-le-temps

observations :
	Plus il y a d'it�rations(m�thode jouer), 
	plus le temps de traitement est long

	plus la profondeur est importante 
	plus le temps de traitement est long .

#Alpha-Beta
	
6.L�algorithme AlphaBeta avec une profondeur d'essaye initiale = 5

7.D'apres mes testes j'ai constat� que l'algorithme alpha-beta
	est plus vite que celui de maximin en temps de traitement.


PS : j'ai fait les compteurs (question 3 et 4) pour la section
	de alpha-beta
