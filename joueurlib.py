#!/usr/bin/python
# coding: utf8


import interfacelib
import numpy as np
import time



class Joueur:
	def __init__(self, partie, couleur, opts={}):
		self.couleur = couleur
		self.jeu = partie
		self.opts = opts

	def demande_coup(self):
		pass


class Humain(Joueur):

	def demande_coup(self):
		pass



class IA(Joueur):

	def __init__(self, partie, couleur, opts={}):
		super().__init__(partie,couleur,opts)
		self.temps_exe = 0
		self.nb_appels_jouer = 0
		


class Random(IA):

	def __init__(self, partie, couleur, opts={}):
		super().__init__(partie,couleur,opts)

	def demande_coup(self):
		couleurval = interfacelib.couleur_to_couleurval(self.couleur)
		listDesCoups = self.jeu.plateau.liste_coups_valides(couleurval)
		if(listDesCoups==[]):
			return []
		i = (int) (np.random.random_integers(0,len(listDesCoups)-1,1))
		return listDesCoups[i]



class MinMax(IA):
	
	compteurJouer = 0

	def __init__(self, partie, couleur, opts={}):
		super().__init__(partie,couleur,opts)

	def demande_coup(self):
		temps_debut = time.time()
		couleurval = interfacelib.couleur_to_couleurval(self.couleur)
		listDesCoups = self.jeu.plateau.liste_coups_valides(couleurval)
		max_ = True
		profondeur = 5
		meilleureValeur = -100000
		coupChoisi =[]
		if(listDesCoups==[]):
			return []
			
		nouveauPlateau = self.jeu.plateau.copie()
		for coup in listDesCoups:
			nouveauPlateau.jouer(coup,couleurval)
			self.compteurJouer+=1	
			val = self.minmax(profondeur - 1,nouveauPlateau, not max_)
			nouveauPlateau = self.jeu.plateau.copie()
			if(val >= meilleureValeur):
				meilleureValeur=val
				coupChoisi= coup
		
		temps_fin = time.time()
		print("MinMax : " )
		print("La profondeur choisit = ",profondeur)
		print("La methode jouer est utilisée ",self.compteurJouer," fois." )
		print ("l'operation a durée : ",temps_fin - temps_debut,"\n")
		return coupChoisi
		
	def evaluer(self,plateau):
		score_noir = 0
		score_blanc = 0
		for i in range(8):
			for j in range(8):
				if(plateau.tableau_cases[i][j] == 1):
					score_noir = score_noir + 1
				elif(plateau.tableau_cases[i][j] == -1):
					score_blanc = score_blanc + 1
							
		if(self.couleur == 'noir'):						
			return score_noir - score_blanc
		else:
			return score_blanc - score_noir			
		
					
	def minmax(self,profondeur,plateau,max_):
		if (profondeur == 0 ):
			return self.evaluer(plateau)

		couleurval = interfacelib.couleur_to_couleurval(self.couleur)
		listDesCoups = plateau.liste_coups_valides(couleurval)

		if (max_) :
			meilleureValeur = -100000
			nouveauPlateau = plateau.copie()	
			for coup in listDesCoups:
				nouveauPlateau.jouer(coup, couleurval)	
				self.compteurJouer += 1	
				val = self.minmax(profondeur - 1,nouveauPlateau, max_)
				nouveauPlateau = plateau.copie()
				meilleureValeur = max(val,meilleureValeur)
      
			return meilleureValeur
		else :
			meilleureValeur = 100000
			nouveauPlateau = plateau.copie()
			for coup in listDesCoups:
				nouveauPlateau.jouer(coup,couleurval)	
				self.compteurJouer+=1	
				val = self.minmax(profondeur - 1,nouveauPlateau, not max_)
				nouveauPlateau = plateau.copie()
				meilleureValeur = min(val,meilleureValeur)
      
			return meilleureValeur
		  
      


class AlphaBeta(IA):
	
	compteurJouer = 0

	def __init__(self, partie, couleur, opts={}):
		super().__init__(partie,couleur,opts)

	def demande_coup(self):
		temps_debut = time.time()
		couleurval = interfacelib.couleur_to_couleurval(self.couleur)
		listDesCoups = self.jeu.plateau.liste_coups_valides(couleurval)
		max_ = True
		profondeur = 50
		meilleureValeur = -100000
		coupChoisi =[]
		if(listDesCoups==[]):
			return []
			
		nouveauPlateau = self.jeu.plateau.copie()
		for coup in listDesCoups:
			nouveauPlateau.jouer(coup,couleurval)
			self.compteurJouer+=1	
			val = self.alpha_beta(profondeur - 1,nouveauPlateau,-1000000,1000000,not max_)
			nouveauPlateau = self.jeu.plateau.copie()
			if(val >= meilleureValeur):
				meilleureValeur=val
				coupChoisi= coup
		
		temps_fin = time.time()
		print("Alpha Beta : " )
		print("La profondeur choisit = ",profondeur)
		print("La methode jouer est utilisée ",self.compteurJouer," fois." )
		print ("l'operation a durée : ",temps_fin - temps_debut,"\n")
		return coupChoisi
		
	def evaluer(self,plateau):
		score_noir=0
		score_blanc=0
		for i in range(8):
			for j in range(8):
				if(plateau.tableau_cases[i][j]==1):
					score_noir+=1
				elif(plateau.tableau_cases[i][j]==-1):
					score_blanc+=1		
		
		if(self.couleur=='noir'):						
			return score_noir-score_blanc
		else:
			return score_blanc-score_noir	
	
	
					
	def alpha_beta(self,profondeur,plateau,alpha,beta,max_):
		if (profondeur==0 ):
				return self.evaluer(plateau)

		couleurval = interfacelib.couleur_to_couleurval(self.couleur)
		listDesCoups = plateau.liste_coups_valides(couleurval)

		if (max_) :
			meilleureValeur = -100000
			nouveauPlateau = plateau.copie()	
			for coup in listDesCoups:
				nouveauPlateau.jouer(coup,couleurval)	
				self.compteurJouer+=1	
				val = self.alpha_beta(profondeur - 1,nouveauPlateau,alpha,beta, max_)
				nouveauPlateau = plateau.copie()
				meilleureValeur = max(val,meilleureValeur)
				alpha = max(alpha,meilleureValeur)
				if(beta <= alpha):
					return meilleureValeur
            
      
			return meilleureValeur
		else :
			meilleureValeur = 100000
			nouveauPlateau = plateau.copie()
			for coup in listDesCoups:
				nouveauPlateau.jouer(coup,couleurval)	
				self.compteurJouer += 1	
				val = self.alpha_beta(profondeur - 1,nouveauPlateau,alpha,beta, not max_)
				nouveauPlateau = plateau.copie()
				meilleureValeur = min(val,meilleureValeur)
				beta = min(alpha,meilleureValeur)
				if(beta <= alpha):
					return meilleureValeur
      
			return meilleureValeur
		  
     


